#include <LiquidCrystal.h>
#include <RCSwitch.h>


int bt_replay=5;

RCSwitch receiver = RCSwitch();
RCSwitch transmitter = RCSwitch();

LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

unsigned long value=0; //data to replay
int length=0;  //data length to replay
int protocol=0; //data protocol to replay
int pulse=0; //data pulse
int get=0;


void setup() {
  Serial.begin(9600);
  
  lcd.begin(16, 2);
  lcd.print("Scanning");
    
  pinMode(bt_replay, INPUT);
  pinMode(3, OUTPUT);
  digitalWrite(3, HIGH);

  receiver.enableReceive(0);
  
}

void loop() {
  if (receiver.available()) {
    get=1;
    Serial.print("Value: ");
    value=receiver.getReceivedValue();
    Serial.print(receiver.getReceivedValue() );
    Serial.print(" / ");
    Serial.print(receiver.getReceivedValue(), BIN);
    
    Serial.print(" Length: ");
    length=receiver.getReceivedBitlength();
    Serial.print(receiver.getReceivedBitlength() );
    
    Serial.print(" Delay: ");
    pulse=receiver.getReceivedDelay();
    Serial.print(receiver.getReceivedDelay() );
    
    Serial.print(" Protocol: ");
    protocol=receiver.getReceivedProtocol();
    Serial.println(receiver.getReceivedProtocol());
  }
  if (get == 1) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Get:");
    lcd.setCursor(0, 1);
    lcd.print(value);
    lcd.display();
  }

  if (!digitalRead(bt_replay)) {
    Serial.print("btn");
    Serial.println("");
    if (value != 0) {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Replay...");
      lcd.setCursor(0,1);
      lcd.print("        ");
    
      receiver.disableReceive();
      transmitter.enableTransmit(13);
      transmitter.setProtocol(protocol);
      transmitter.setRepeatTransmit(15);
      transmitter.setPulseLength(pulse-14);
      transmitter.send(value, length);
      delay(3000);
      
      value=0;
      protocol=0;
      pulse=0;
      length=0;
      get=0;
      
      receiver.enableReceive(0);
      lcd.setCursor(0,0);
      lcd.print("Scanning");
    }
  }
  delay(1000);
}

