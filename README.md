# README #

This repo contains the source code of the project mention in my paper called: "433Mhz ASK signal analysis – Wireless door bell adventure". It is available in the pdf folder.


### The folders ###

* pdf/: contains the pdf document of my work
* arduino/: contains the source code used in the arduino project
* GNU_Radio_Companion/: contains the GRC schema

Paul.